﻿using BackendDotNet.Common.Entities;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BackendDotNet.Database.Abstract;
using BackendDotNet.Database.Concrete;

namespace BackendDotNet.Web.Controllers.v0
{
    public class UserController : ApiController
    {
        private readonly UserRepository _repository;

        public UserController(IDbConnector connector)
        {
            _repository = new UserRepository(connector);
        }

        // GET api/user
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }

        // GET api/user/5
        public HttpResponseMessage Get(long id)
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }

        // POST api/user
        public HttpResponseMessage Post([FromBody]User value)
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }

        // PUT api/user/5
        public HttpResponseMessage Put(long id, [FromBody]User value)
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }

        // DELETE api/user/5
        public HttpResponseMessage Delete(long id)
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }

        public HttpResponseMessage Options()
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }
    }
}
