﻿using BackendDotNet.Common.Entities;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BackendDotNet.Database.Abstract;
using BackendDotNet.Database.Concrete;

namespace BackendDotNet.Web.Controllers.v0
{
    public class SchoolController : ApiController
    {
        private readonly SchoolRepository _repository;

        public SchoolController(IDbConnector connector)
        {
            _repository = new SchoolRepository(connector);
        }

        // GET api/school
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }

        // GET api/school/5
        public HttpResponseMessage Get(long id)
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }

        // POST api/school
        public HttpResponseMessage Post([FromBody]School value)
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }

        // PUT api/school/5
        public HttpResponseMessage Put(long id, [FromBody]School value)
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }

        // DELETE api/school/5
        public HttpResponseMessage Delete(long id)
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }

        public HttpResponseMessage Options()
        {
            return Request.CreateResponse(HttpStatusCode.MovedPermanently, "/api/v1/");
        }
    }
}
