﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BackendDotNet.Common.Entities;
using BackendDotNet.Database.Abstract;
using BackendDotNet.Database.Concrete;
using PagedList;

namespace BackendDotNet.Web.Controllers.v2
{
    [System.Web.Http.Authorize]
    public class RoleController : ApiController
    {
        private readonly RoleRepository _roleRepository;

        public RoleController(IDbConnector connector)
        {
            _roleRepository = new RoleRepository(connector);
        }

        public HttpResponseMessage Get(long? school = null, int page = 1, int count = 10)
        {
            var tmp = _roleRepository.GetAll();

            if (school.HasValue)
            {
                tmp = tmp.Where(x => x.School.ID == school).ToList();
            }

            var pagedList = new PagedList<Role>(tmp, page, count);
            
            return Request.CreateResponse(HttpStatusCode.OK, pagedList);
        }

        public HttpResponseMessage Get(long id)
        {
            var tmp = _roleRepository.Find(id);
            if (tmp == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return Request.CreateResponse(HttpStatusCode.OK, tmp);
        }

        public HttpResponseMessage Post(Role status)
        {
            if (ModelState.IsValid && status.ID == 0)
            {
                var tmp = _roleRepository.Add(status);

                if (tmp == null)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError);
                }

                return Request.CreateResponse(HttpStatusCode.Created, tmp);
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        public HttpResponseMessage Put(long id, Role status)
        {
            return Request.CreateResponse(HttpStatusCode.MethodNotAllowed);
        }

        public HttpResponseMessage Delete(long id)
        {
            return Request.CreateResponse(HttpStatusCode.MethodNotAllowed);
        }

        /// <summary>
        /// OPTIONS /api/v1/
        /// Returns the options for this controller.
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage Options()
        {
            return Request.CreateResponse(HttpStatusCode.OK, "API: http://csci321f14.pbworks.com/w/page/88617830/Role");
        }
    }
}
