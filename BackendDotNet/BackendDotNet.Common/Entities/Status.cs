﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BackendDotNet.Common.Entities
{
    public class Status
    {
        public long ID { get; set; }

        public School School { get; set; }

        public string Name { get; set; }
    }
}
