﻿using System.Collections.Generic;

namespace BackendDotNet.Common.Entities
{
    public class Report
    {
        public long ID { get; set; }

        public User User { get; set; }

        public Alert Alert { get; set; }

        public Status Status { get; set; }

        public School School { get; set; }

        public bool Flag { get; set; }

        public virtual IList<Location> Locations { get; set; }

        public virtual IList<Picture> Pictures { get; set; }
    }
}