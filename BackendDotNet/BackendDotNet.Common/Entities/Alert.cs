﻿namespace BackendDotNet.Common.Entities
{
    public class Alert
    {
        public long ID { get; set; }

        public School School { get; set; }

        public string Title { get; set; }

        public uint Color { get; set; }

        public int Priority { get; set; }

        public bool Enabled { get; set; }
    }
}