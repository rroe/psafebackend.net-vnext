﻿using System;
using System.Collections.Generic;

namespace BackendDotNet.Database.Abstract
{
    /// <summary>
    /// Same as <see cref="IRepository{T,TK}"/> but allows us to be lazy and not specify a key as long as it is our default key type (long).
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T> : IRepository<T, long>
    {
    }

    /// <summary>
    /// Base interface for Repositories.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TK"></typeparam>
    public interface IRepository<T, in TK> : IDisposable, IEnumerable<T>
    {
        IDbConnector Source { get; }

        T this[TK id] { get; set; }

        T Add(T entity);

        T Remove(T entity);

        T Find(TK key);

        T Update(T entity);

        IList<T> GetAll(Func<T, bool> filter = null);
    }
}