﻿using BackendDotNet.Common.Entities;
using BackendDotNet.Database.Abstract;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BackendDotNet.Database.Concrete
{
    public class PictureRepository : IRepository<Picture>
    {
        public PictureRepository(IDbConnector connector)
        {
            Source = connector;
        }

        public void Dispose()
        {
            Finalize(true);
            GC.SuppressFinalize(this);
        }

        protected void Finalize(bool isDisposing)
        {
            if (isDisposing)
            {
                Source.Dispose();
            }
        }

        public IEnumerator<Picture> GetEnumerator()
        {
            return Source.Pictures.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IDbConnector Source { get; private set; }

        public Picture this[long id]
        {
            get { return Find(id); }
            set
            {
                if (Find(id) == null)
                    Add(value);
                else
                    Update(value);
            }
        }

        public Picture Add(Picture entity)
        {
            try
            {
                Source.Pictures.Add(entity);
                return entity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Source.SaveChanges();
            }
        }

        public Picture Remove(Picture entity)
        {
            try
            {
                Source.Pictures.Remove(entity);
                return entity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Source.SaveChanges();
            }
        }

        public Picture Find(long key)
        {
            try
            {
                return Source.Pictures.First(x => x.ID == key);
            }
            catch
            {
                return null;
            }
        }

        public Picture Update(Picture entity)
        {
            try
            {
                var tmp = Source.Pictures.FirstOrDefault(x => x.ID == entity.ID);
                tmp = entity;
                return tmp;
            }
            catch
            {
                return null;
            }
            finally
            {
                Source.SaveChanges();
            }
        }

        public IList<Picture> GetAll(Func<Picture, bool> filter = null)
        {
            if (filter == null)
            {
                return Source.Pictures.ToList();
            }

            return Source.Pictures.Where(filter.Invoke).ToList();
        }
    }
}