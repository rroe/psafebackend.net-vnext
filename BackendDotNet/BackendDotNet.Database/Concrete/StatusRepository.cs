﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using BackendDotNet.Common.Entities;
using BackendDotNet.Database.Abstract;

namespace BackendDotNet.Database.Concrete
{
    public class StatusRepository : IRepository<Status>
    {
        public StatusRepository(IDbConnector connector)
        {
            Source = connector;
        }

        public void Dispose()
        {
            Finalize(true);
            GC.SuppressFinalize(this);
        }

        protected void Finalize(bool isDisposing)
        {
            if (isDisposing)
            {
                Source.Dispose();
            }
        }

        public IEnumerator<Status> GetEnumerator()
        {
            return Source.Statuses.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IDbConnector Source { get; private set; }

        public Status this[long id]
        {
            get { return Find(id); }
            set
            {
                if (Find(id) == null)
                    Add(value);
                else
                    Update(value);
            }
        }

        public Status Add(Status entity)
        {
            try
            {
                var tmpSchool = Source.Schools.First(x => x.ID == entity.School.ID);
                entity.School = tmpSchool;
                Source.Statuses.Add(entity);
                return entity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Source.SaveChanges();
            }
        }

        public Status Remove(Status entity)
        {
            try
            {
                Source.Statuses.Remove(entity);
                return entity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Source.SaveChanges();
            }
        }

        public Status Find(long key)
        {
            try
            {
                return Source.Statuses.Include(x=>x.School).First(x => x.ID == key);
            }
            catch
            {
                return null;
            }
            finally
            {
                Source.SaveChanges();
            }
        }

        public Status Update(Status entity)
        {
            try
            {
                var tmp = Source.Statuses.FirstOrDefault(x => x.ID == entity.ID);
                tmp.Name = entity.Name;
                return tmp;
            }
            catch
            {
                return null;
            }
            finally
            {
                Source.SaveChanges();
            }
        }

        public IList<Status> GetAll(Func<Status, bool> filter = null)
        {
            if (filter == null)
            {
                return Source.Statuses.Include(x => x.School).ToList();
            }

            return Source.Statuses.Include(x => x.School).Where(filter.Invoke).ToList();
        }
    }
}
