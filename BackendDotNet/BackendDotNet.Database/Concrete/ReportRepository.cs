﻿using System.Data.Entity;
using BackendDotNet.Common.Entities;
using BackendDotNet.Database.Abstract;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BackendDotNet.Database.Concrete
{
    public class ReportRepository : IRepository<Report>
    {
        public ReportRepository(IDbConnector connector)
        {
            Source = connector;
        }

        public void Dispose()
        {
            Finalize(true);
            GC.SuppressFinalize(this);
        }

        protected void Finalize(bool isDisposing)
        {
            if (isDisposing)
            {
                Source.Dispose();
            }
        }

        public IEnumerator<Report> GetEnumerator()
        {
            return Source.Reports.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IDbConnector Source { get; private set; }

        public Report this[long id]
        {
            get { return Find(id); }
            set
            {
                if (Find(id) == null)
                    Add(value);
                else
                    Update(value);
            }
        }

        public Report Add(Report entity)
        {
            try
            {
                var tmpUser = Source.Users.First(x => x.ID == entity.User.ID);
                entity.User = tmpUser;
                var tmpAlert = Source.Alerts.First(x => x.ID == entity.Alert.ID);
                entity.Alert = tmpAlert;
                var tmpStatus = Source.Statuses.First(x => x.ID == entity.Status.ID);
                entity.Status = tmpStatus;
                var tmpSchool = Source.Schools.First(x => x.ID == entity.School.ID);
                entity.School = tmpSchool;
                Source.Reports.Add(entity);
                return entity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Source.SaveChanges();
            }
        }

        public Report Remove(Report entity)
        {
            try
            {
                Source.Reports.Remove(entity);
                return entity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Source.SaveChanges();
            }
        }

        public Report Find(long key)
        {
            try
            {
                return Source.Reports.Include(x => x.School).Include(x => x.Alert).Include(x => x.Status).Include(x => x.User).Include(x => x.Locations).First(x => x.ID == key);
            }
            catch
            {
                return null;
            }
        }

        public Report Update(Report entity)
        {
            try
            {
                var tmp = Source.Reports.FirstOrDefault(x => x.ID == entity.ID);
                tmp.Locations = entity.Locations;
                tmp.Pictures = entity.Pictures ?? tmp.Pictures;
                tmp.Status = entity.Status ?? tmp.Status;
                tmp.Flag = entity.Flag;
                return tmp;
            }
            catch
            {
                return null;
            }
            finally
            {
                Source.SaveChanges();
            }
        }

        public IList<Report> GetAll(Func<Report, bool> filter = null)
        {
            if (filter == null)
            {
                return Source.Reports.Include(x => x.School).Include(x => x.Alert).Include(x => x.Status).Include(x => x.User).Include(x => x.Locations).ToList();
            }

            return Source.Reports.Include(x => x.School).Include(x => x.Alert).Include(x => x.Status).Include(x => x.User).Include(x => x.Locations).Where(filter.Invoke).ToList();
        }
    }
}