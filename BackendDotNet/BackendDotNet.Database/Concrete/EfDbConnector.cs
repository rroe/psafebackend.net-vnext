﻿using BackendDotNet.Common.Entities;
using BackendDotNet.Database.Abstract;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BackendDotNet.Database.Concrete
{
    public class EfDbConnector : DbContext, IDbConnector
    {
        public EfDbConnector()
            : base("DefaultConnection")
        {
            
        }

        public IDbSet<Report> Reports { get; set; }

        public IDbSet<Alert> Alerts { get; set; }

        public IDbSet<Location> Locations { get; set; }

        public IDbSet<Picture> Pictures { get; set; }

        public IDbSet<School> Schools { get; set; }

        public IDbSet<User> Users { get; set; }

        public IDbSet<Role> Roles { get; set; }

        public IDbSet<Status> Statuses { get; set; } 
    }
}