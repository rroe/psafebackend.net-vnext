﻿using System.Configuration;
using System.Data.Entity;
using BackendDotNet.Common.Entities;
using BackendDotNet.Database.Abstract;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BackendDotNet.Database.Concrete
{
    public class UserRepository : IRepository<User>
    {
        public UserRepository(IDbConnector connector)
        {
            Source = connector;
        }

        public void Dispose()
        {
            Finalize(true);
            GC.SuppressFinalize(this);
        }

        protected void Finalize(bool isDisposing)
        {
            if (isDisposing)
            {
                Source.Dispose();
            }
        }

        public IEnumerator<User> GetEnumerator()
        {
            return Source.Users.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IDbConnector Source { get; private set; }

        public User this[long id]
        {
            get { return Find(id); }
            set
            {
                if (Find(id) == null)
                    Add(value);
                else
                    Update(value);
            }
        }

        public User Add(User entity)
        {
            try
            {
                var tmpSchool = Source.Schools.First(x => x.ID == entity.School.ID);
                entity.School = tmpSchool;
                var tmpPiccture = Source.Pictures.First(x => x.ID == entity.Picture.ID);
                entity.Picture = tmpPiccture;
                var tmpRole = Source.Roles.First(x => x.ID == entity.Role.ID);
                entity.Role = tmpRole;

                Source.Users.Add(entity);
                return entity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Source.SaveChanges();
            }
        }

        public User Remove(User entity)
        {
            try
            {
                Source.Users.Remove(entity);
                return entity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Source.SaveChanges();
            }
        }

        public User Find(long key)
        {
            try
            {
                return Source.Users.Include(x=>x.Role).Include(x=>x.School).Include(x=>x.Picture).First(x => x.ID == key);
            }
            catch
            {
                return null;
            }
        }

        public User Update(User entity)
        {
            try
            {
                var tmp = Source.Users.FirstOrDefault(x => x.ID == entity.ID);
                tmp.FName = entity.FName;
                tmp.LName = entity.LName;
                tmp.Password = entity.Password;
                tmp.OrganizationIdentifier = entity.OrganizationIdentifier;
                tmp.ClientSecretToken = entity.ClientSecretToken ?? tmp.ClientSecretToken;
                return tmp;
            }
            catch
            {
                return null;
            }
            finally
            {
                Source.SaveChanges();
            }
        }

        public IList<User> GetAll(Func<User, bool> filter = null)
        {
            if (filter == null)
            {
                return Source.Users.Include(x => x.Role).Include(x => x.School).Include(x => x.Picture).ToList();
            }

            return Source.Users.Include(x => x.Role).Include(x => x.School).Include(x => x.Picture).Where(filter.Invoke).ToList();
        }
    }
}