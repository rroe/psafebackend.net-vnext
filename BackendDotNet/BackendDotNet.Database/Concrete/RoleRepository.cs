﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using BackendDotNet.Common.Entities;
using BackendDotNet.Database.Abstract;

namespace BackendDotNet.Database.Concrete
{
    public class RoleRepository : IRepository<Role>
    {
        public RoleRepository(IDbConnector connector)
        {
            Source = connector;
        }

        public void Dispose()
        {
            Finalize(true);
            GC.SuppressFinalize(this);
        }

        protected void Finalize(bool isDisposing)
        {
            if (isDisposing)
            {
                Source.Dispose();
            }
        }

        public IEnumerator<Role> GetEnumerator()
        {
            return Source.Roles.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IDbConnector Source { get; private set; }

        public Role this[long id]
        {
            get { return Find(id); }
            set
            {
                if (Find(id) == null)
                    Add(value);
                else
                    Update(value);
            }
        }

        public Role Add(Role entity)
        {
            try
            {
                var tmpSchool = Source.Schools.First(x => x.ID == entity.School.ID);
                entity.School = tmpSchool;
                Source.Roles.Add(entity);
                return entity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Source.SaveChanges();
            }
        }

        public Role Remove(Role entity)
        {
            try
            {
                Source.Roles.Remove(entity);
                return entity;
            }
            catch
            {
                return null;
            }
            finally
            {
                Source.SaveChanges();
            }
        }

        public Role Find(long key)
        {
            try
            {
                return Source.Roles.Include(x => x.School).First(x => x.ID == key);
            }
            catch
            {
                return null;
            }
            finally
            {
                Source.SaveChanges();
            }
        }

        public Role Update(Role entity)
        {
            try
            {
                var tmp = Source.Roles.FirstOrDefault(x => x.ID == entity.ID);
                tmp.Name = entity.Name;
                return tmp;
            }
            catch
            {
                return null;
            }
            finally
            {
                Source.SaveChanges();
            }
        }

        public IList<Role> GetAll(Func<Role, bool> filter = null)
        {
            if (filter == null)
            {
                return Source.Roles.Include(x => x.School).ToList();
            }

            return Source.Roles.Include(x => x.School).Where(filter.Invoke).ToList();
        }
    }
}
