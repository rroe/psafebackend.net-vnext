namespace BackendDotNet.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReportSchool : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reports", "School_ID", c => c.Long());
            CreateIndex("dbo.Reports", "School_ID");
            AddForeignKey("dbo.Reports", "School_ID", "dbo.Schools", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reports", "School_ID", "dbo.Schools");
            DropIndex("dbo.Reports", new[] { "School_ID" });
            DropColumn("dbo.Reports", "School_ID");
        }
    }
}
