namespace BackendDotNet.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PicturesInReports : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pictures", "Report_ID", c => c.Long());
            CreateIndex("dbo.Pictures", "Report_ID");
            AddForeignKey("dbo.Pictures", "Report_ID", "dbo.Reports", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pictures", "Report_ID", "dbo.Reports");
            DropIndex("dbo.Pictures", new[] { "Report_ID" });
            DropColumn("dbo.Pictures", "Report_ID");
        }
    }
}
