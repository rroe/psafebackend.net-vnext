namespace BackendDotNet.Database.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<BackendDotNet.Database.Concrete.EfDbConnector>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "BackendDotNet.Database.Concrete.EfDbConnector";
        }

        protected override void Seed(BackendDotNet.Database.Concrete.EfDbConnector context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
