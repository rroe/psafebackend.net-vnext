﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BackendDotNet.Common.Entities;
using BackendDotNet.Database.Abstract;
using BackendDotNet.Database.Concrete;
using PagedList;

namespace BackendDotNet.Controllers.v1
{
    public class StatusController : ApiController
    {
        private readonly StatusRepository _statusRepository;

        public StatusController(IDbConnector connector)
        {
            _statusRepository = new StatusRepository(connector);
        }

        public HttpResponseMessage Get(long? school = null, int page = 1, int count = 10)
        {
            var tmp = _statusRepository.GetAll();

            if (school.HasValue)
            {
                tmp = tmp.Where(x => x.School.ID == school).ToList();
            }

            var pagedList = new PagedList<Status>(tmp, page, count);

            return Request.CreateResponse(HttpStatusCode.OK, pagedList);
        }

        public HttpResponseMessage Get(long id)
        {
            var tmp = _statusRepository.Find(id);
            if (tmp == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return Request.CreateResponse(HttpStatusCode.OK, tmp);
        }

        public HttpResponseMessage Post(Status status)
        {
            if (ModelState.IsValid && status.ID == 0)
            {
                var tmp = _statusRepository.Add(status);

                if (tmp == null)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError);
                }

                return Request.CreateResponse(HttpStatusCode.Created, tmp);
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        public HttpResponseMessage Put(long id, Status status)
        {
            return Request.CreateResponse(HttpStatusCode.MethodNotAllowed);
        }

        public HttpResponseMessage Delete(long id)
        {
            return Request.CreateResponse(HttpStatusCode.MethodNotAllowed);
        }

        /// <summary>
        /// OPTIONS /api/v1/
        /// Returns the options for this controller.
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage Options()
        {
            return Request.CreateResponse(HttpStatusCode.OK, "API: http://csci321f14.pbworks.com/w/page/88617974/Status");
        }
    }
}
