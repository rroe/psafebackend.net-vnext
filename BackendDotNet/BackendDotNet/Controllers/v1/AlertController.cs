﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Caching;
using System.Web.Http;
using System.Web.Mvc;
using BackendDotNet.Common.Entities;
using BackendDotNet.Database.Abstract;
using BackendDotNet.Database.Concrete;
using PagedList;

namespace BackendDotNet.Controllers.v1
{
    public class AlertController : ApiController
    {
        private readonly AlertRepository _alertRepository;

        public AlertController(IDbConnector dbConnector)
        {
            _alertRepository = new AlertRepository(dbConnector);
        }

        /// <summary>
        /// GET /api/v1/Alert
        /// Gets all alerts
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage Get(long? school = null, int page = 1, int count = 10)
        {
            var alerts = _alertRepository.GetAll();

            if (school.HasValue)
            {
                alerts = alerts.Where(x => x.School.ID == school).ToList();
            }

            var pagedList = new PagedList<Alert>(alerts, page, count);

            return Request.CreateResponse(HttpStatusCode.OK, pagedList);
        }

        /// <summary>
        /// GET /api/v1/Alert/{id}
        /// Gets the alert specified with {id}
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public HttpResponseMessage Get(long id)
        {
            var alert = _alertRepository.Find(id);
            if (alert == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return Request.CreateResponse(HttpStatusCode.OK, alert);
        }

        /// <summary>
        /// PUT /api/v1/Alert/{id}
        /// Not allowed.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="alert"></param>
        /// <returns></returns>
        public HttpResponseMessage Put(long id, Alert alert)
        {
            return Request.CreateResponse(HttpStatusCode.MethodNotAllowed);
        }

        /// <summary>
        /// POST /api/v1/Alert
        /// Adds a new Alert
        /// </summary>
        /// <param name="alert"></param>
        /// <returns></returns>
        public HttpResponseMessage Post(Alert alert)
        {
            if (alert.ID != 0)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            if (ModelState.IsValid)
            {
                var tmp = _alertRepository.Add(alert);

                if (tmp == null)
                {
                    return Request.CreateResponse(HttpStatusCode.ExpectationFailed);
                }
                return Request.CreateResponse(HttpStatusCode.Created, tmp);
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
        }

        /// <summary>
        /// DELETE /api/v1/Alert/{id}
        /// Marks the specified alert as Inactive (Active = false)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public HttpResponseMessage Delete(long id)
        {
            var alert = _alertRepository.Find(id);
            if (alert.Enabled == false)
            {
                return Request.CreateResponse(HttpStatusCode.NotModified);
            }

            alert.Enabled = false;
            alert = _alertRepository.Update(alert);
            return Request.CreateResponse(HttpStatusCode.OK, alert);
        }

        /// <summary>
        /// Returns the options for this controller.
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage Options()
        {
            return Request.CreateResponse(HttpStatusCode.OK, "API: http://csci321f14.pbworks.com/w/page/87779470/Alert");
        }

    }
}
