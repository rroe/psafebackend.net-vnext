﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BackendDotNet.Common.Entities;
using BackendDotNet.Database.Abstract;
using BackendDotNet.Database.Concrete;
using BackendDotNet.Models;
using Microsoft.Ajax.Utilities;
using PagedList;

namespace BackendDotNet.Controllers.v2
{
    public class LoginController : ApiController
    {
        private readonly UserRepository _userRepository;

        public LoginController(IDbConnector dbConnector)
        {
            _userRepository = new UserRepository(dbConnector);
        }

        public HttpResponseMessage Get()
        {
            return new HttpResponseMessage(HttpStatusCode.MethodNotAllowed);
        }


        public HttpResponseMessage Put()
        {
            return Request.CreateResponse(HttpStatusCode.MethodNotAllowed);
        }
        
        /// <summary>
        /// Checks to see if users can log in; if so, sends the Client_Secret back to the person.
        /// </summary>
        /// <param name="alert"></param>
        /// <returns></returns>
        public HttpResponseMessage Post(string username, string password)
        {
            foreach (User user in _userRepository)
            {
                if (user.Username == username)
                {
                    if (password == user.Password)
                    {
                        user.ClientSecretToken = AuthenticationUtilities.GenerateToken();
                        _userRepository.Update(user);
                        return Request.CreateResponse(HttpStatusCode.OK, user);
                    }
                }
            }
            return new HttpResponseMessage(HttpStatusCode.BadRequest);
        }

        /// <summary>
        /// Allows users to log out. Technically not needed, as if they log in again,
        /// then the current token will become invalid. However, still meh.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public HttpResponseMessage Delete(string username, string token)
        {
            string newToken = AuthenticationUtilities.GenerateToken();
            foreach (User user in _userRepository)
            {
                if (username == user.Username)
                {
                    if (token == user.ClientSecretToken)
                    {
                        user.ClientSecretToken = newToken;
                        _userRepository.Update(user);
                        return new HttpResponseMessage(HttpStatusCode.OK);
                    }
                }
            }
            return new HttpResponseMessage(HttpStatusCode.BadRequest);
        }

        public HttpResponseMessage Options()
        {
            return new HttpResponseMessage(HttpStatusCode.MethodNotAllowed);
        }
    }
}
